Buy Janitorial Direct sells equipment and supplies for Janitorial Professionals, Residential and Commercial Carpet Cleaning Professionals, In-House Service Providers, Disaster Restoration Contractors, Abatement Contractors, and Building Service Contractors.

Address: 3750 W Colonial Dr, Orlando, FL 32808, USA
Phone: 407-745-4759
Website: https://www.buyjanitorialdirect.com
